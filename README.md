A winforms .exe that allows for the entering of a competitor's details into a db.


Classes
* CompetitorContext     ->      Context for getting and adding Competitors, Coaches and Teams from/to db
* AddCompetitorForm     ->      Form for filling in the details of the competitor as well as displaying all competitors
* Competitor            ->      Class for EF (many-to-one relationship with Team)
* Coach                 ->      Class for EF (one-to-one relationship with Team)
* Team                  ->      Class for EF (one-to-many relationship with Competitor)
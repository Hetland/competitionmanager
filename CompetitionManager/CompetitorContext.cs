﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompetitionManager
{
    public class CompetitorContext : DbContext
    {
        public DbSet<Competitor> Competitors { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Team> Teams { get; set; }

        //public CompetitorContext(DbContextOptions<CompetitorContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=PC7276\SQLEXPRESS;Database=DBofCompetitors;Trusted_Connection=True;MultipleActiveResultSets=True");
        }
    }
}

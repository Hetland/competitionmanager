﻿namespace CompetitionManager
{
    partial class TeamsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.competitorBoxLabel = new System.Windows.Forms.Label();
            this.teamsBox = new System.Windows.Forms.GroupBox();
            this.deleteTeamButton = new System.Windows.Forms.Button();
            this.editTeamButton = new System.Windows.Forms.Button();
            this.allTeamsListBox = new System.Windows.Forms.ListView();
            this.NameCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CoachNameCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.editTeamBox = new System.Windows.Forms.GroupBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.editTeam = new System.Windows.Forms.Button();
            this.teamNameTextBox = new System.Windows.Forms.TextBox();
            this.coachLabel = new System.Windows.Forms.Label();
            this.editTeamLabel = new System.Windows.Forms.Label();
            this.coachNameTextBox = new System.Windows.Forms.TextBox();
            this.finishedButton = new System.Windows.Forms.Button();
            this.teamsBox.SuspendLayout();
            this.editTeamBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // competitorBoxLabel
            // 
            this.competitorBoxLabel.AutoSize = true;
            this.competitorBoxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.competitorBoxLabel.Location = new System.Drawing.Point(27, 5);
            this.competitorBoxLabel.Name = "competitorBoxLabel";
            this.competitorBoxLabel.Size = new System.Drawing.Size(73, 24);
            this.competitorBoxLabel.TabIndex = 15;
            this.competitorBoxLabel.Text = "Teams";
            // 
            // teamsBox
            // 
            this.teamsBox.Controls.Add(this.deleteTeamButton);
            this.teamsBox.Controls.Add(this.editTeamButton);
            this.teamsBox.Controls.Add(this.allTeamsListBox);
            this.teamsBox.Location = new System.Drawing.Point(11, 27);
            this.teamsBox.Name = "teamsBox";
            this.teamsBox.Size = new System.Drawing.Size(395, 357);
            this.teamsBox.TabIndex = 14;
            this.teamsBox.TabStop = false;
            // 
            // deleteTeamButton
            // 
            this.deleteTeamButton.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteTeamButton.Location = new System.Drawing.Point(208, 317);
            this.deleteTeamButton.Name = "deleteTeamButton";
            this.deleteTeamButton.Size = new System.Drawing.Size(181, 34);
            this.deleteTeamButton.TabIndex = 10;
            this.deleteTeamButton.Text = "Delete";
            this.deleteTeamButton.UseVisualStyleBackColor = true;
            // 
            // editTeamButton
            // 
            this.editTeamButton.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editTeamButton.Location = new System.Drawing.Point(6, 317);
            this.editTeamButton.Name = "editTeamButton";
            this.editTeamButton.Size = new System.Drawing.Size(196, 34);
            this.editTeamButton.TabIndex = 9;
            this.editTeamButton.Text = "Edit";
            this.editTeamButton.UseVisualStyleBackColor = true;
            // 
            // allTeamsListBox
            // 
            this.allTeamsListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.NameCol,
            this.CoachNameCol});
            this.allTeamsListBox.FullRowSelect = true;
            this.allTeamsListBox.HideSelection = false;
            this.allTeamsListBox.Location = new System.Drawing.Point(6, 14);
            this.allTeamsListBox.MultiSelect = false;
            this.allTeamsListBox.Name = "allTeamsListBox";
            this.allTeamsListBox.Size = new System.Drawing.Size(383, 297);
            this.allTeamsListBox.TabIndex = 0;
            this.allTeamsListBox.UseCompatibleStateImageBehavior = false;
            // 
            // NameCol
            // 
            this.NameCol.Text = "Name";
            // 
            // CoachNameCol
            // 
            this.CoachNameCol.Text = "Coach";
            // 
            // editTeamBox
            // 
            this.editTeamBox.Controls.Add(this.coachNameTextBox);
            this.editTeamBox.Controls.Add(this.nameLabel);
            this.editTeamBox.Controls.Add(this.editTeam);
            this.editTeamBox.Controls.Add(this.teamNameTextBox);
            this.editTeamBox.Controls.Add(this.coachLabel);
            this.editTeamBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.editTeamBox.Location = new System.Drawing.Point(426, 126);
            this.editTeamBox.Name = "editTeamBox";
            this.editTeamBox.Size = new System.Drawing.Size(388, 170);
            this.editTeamBox.TabIndex = 17;
            this.editTeamBox.TabStop = false;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(9, 21);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(93, 20);
            this.nameLabel.TabIndex = 5;
            this.nameLabel.Text = "Team name";
            // 
            // editTeam
            // 
            this.editTeam.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editTeam.Location = new System.Drawing.Point(141, 114);
            this.editTeam.Name = "editTeam";
            this.editTeam.Size = new System.Drawing.Size(238, 46);
            this.editTeam.TabIndex = 1;
            this.editTeam.Text = "Update team";
            this.editTeam.UseVisualStyleBackColor = true;
            // 
            // teamNameTextBox
            // 
            this.teamNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teamNameTextBox.Location = new System.Drawing.Point(141, 18);
            this.teamNameTextBox.Name = "teamNameTextBox";
            this.teamNameTextBox.Size = new System.Drawing.Size(238, 26);
            this.teamNameTextBox.TabIndex = 2;
            // 
            // coachLabel
            // 
            this.coachLabel.AutoSize = true;
            this.coachLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coachLabel.Location = new System.Drawing.Point(9, 62);
            this.coachLabel.Name = "coachLabel";
            this.coachLabel.Size = new System.Drawing.Size(116, 20);
            this.coachLabel.TabIndex = 7;
            this.coachLabel.Text = "Name of coach";
            // 
            // editTeamLabel
            // 
            this.editTeamLabel.AutoSize = true;
            this.editTeamLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editTeamLabel.Location = new System.Drawing.Point(580, 77);
            this.editTeamLabel.Name = "editTeamLabel";
            this.editTeamLabel.Size = new System.Drawing.Size(105, 24);
            this.editTeamLabel.TabIndex = 16;
            this.editTeamLabel.Text = "Edit Team";
            // 
            // coachNameTextBox
            // 
            this.coachNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coachNameTextBox.Location = new System.Drawing.Point(141, 58);
            this.coachNameTextBox.Name = "coachNameTextBox";
            this.coachNameTextBox.Size = new System.Drawing.Size(238, 26);
            this.coachNameTextBox.TabIndex = 8;
            // 
            // finishedButton
            // 
            this.finishedButton.Font = new System.Drawing.Font("Rockwell", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.finishedButton.Location = new System.Drawing.Point(284, 408);
            this.finishedButton.Name = "finishedButton";
            this.finishedButton.Size = new System.Drawing.Size(280, 46);
            this.finishedButton.TabIndex = 9;
            this.finishedButton.Text = "Go back to competitors";
            this.finishedButton.UseVisualStyleBackColor = true;
            this.finishedButton.Click += new System.EventHandler(this.finishedButton_Click);
            // 
            // TeamsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 475);
            this.Controls.Add(this.finishedButton);
            this.Controls.Add(this.editTeamBox);
            this.Controls.Add(this.editTeamLabel);
            this.Controls.Add(this.competitorBoxLabel);
            this.Controls.Add(this.teamsBox);
            this.Name = "TeamsForm";
            this.Text = "TeamsForm";
            this.teamsBox.ResumeLayout(false);
            this.editTeamBox.ResumeLayout(false);
            this.editTeamBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label competitorBoxLabel;
        private System.Windows.Forms.GroupBox teamsBox;
        private System.Windows.Forms.Button deleteTeamButton;
        private System.Windows.Forms.Button editTeamButton;
        private System.Windows.Forms.ListView allTeamsListBox;
        private System.Windows.Forms.ColumnHeader NameCol;
        private System.Windows.Forms.ColumnHeader CoachNameCol;
        private System.Windows.Forms.GroupBox editTeamBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Button editTeam;
        private System.Windows.Forms.TextBox teamNameTextBox;
        private System.Windows.Forms.Label coachLabel;
        private System.Windows.Forms.Label editTeamLabel;
        private System.Windows.Forms.TextBox coachNameTextBox;
        private System.Windows.Forms.Button finishedButton;
    }
}
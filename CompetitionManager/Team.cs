﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompetitionManager
{
    public class Team
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Coach SupervisingCoach { get; set; }
        public ICollection<Competitor> Competitors { get; set; }

        public Team()
        {
            Name = "CNerdYO";
        }

        public Team(string name)
        {
            Name = name;
        }
    }
}

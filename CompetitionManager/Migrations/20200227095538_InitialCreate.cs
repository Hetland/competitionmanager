﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CompetitionManager.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Competitors",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: false),
                    CurrentResidence = table.Column<string>(nullable: true),
                    YearsPlayedVideogames = table.Column<float>(nullable: false),
                    GoodWithComputers = table.Column<bool>(nullable: false),
                    IsIntrovert = table.Column<bool>(nullable: false),
                    MangaOrAnime = table.Column<bool>(nullable: false),
                    LovesSciFi = table.Column<bool>(nullable: false),
                    CollectsCards = table.Column<bool>(nullable: false),
                    LivesAtHome = table.Column<bool>(nullable: false),
                    HasGirlfriend = table.Column<bool>(nullable: false),
                    LovesScience = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Competitors", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Competitors");
        }
    }
}

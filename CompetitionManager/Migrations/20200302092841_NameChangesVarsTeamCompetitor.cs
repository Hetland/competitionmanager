﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CompetitionManager.Migrations
{
    public partial class NameChangesVarsTeamCompetitor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competitors_Teams_TeamInID",
                table: "Competitors");

            migrationBuilder.DropIndex(
                name: "IX_Competitors_TeamInID",
                table: "Competitors");

            migrationBuilder.DropColumn(
                name: "TeamInID",
                table: "Competitors");

            migrationBuilder.AddColumn<int>(
                name: "TeamId",
                table: "Competitors",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Competitors_TeamId",
                table: "Competitors",
                column: "TeamId");

            migrationBuilder.AddForeignKey(
                name: "FK_Competitors_Teams_TeamId",
                table: "Competitors",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competitors_Teams_TeamId",
                table: "Competitors");

            migrationBuilder.DropIndex(
                name: "IX_Competitors_TeamId",
                table: "Competitors");

            migrationBuilder.DropColumn(
                name: "TeamId",
                table: "Competitors");

            migrationBuilder.AddColumn<int>(
                name: "TeamInID",
                table: "Competitors",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Competitors_TeamInID",
                table: "Competitors",
                column: "TeamInID");

            migrationBuilder.AddForeignKey(
                name: "FK_Competitors_Teams_TeamInID",
                table: "Competitors",
                column: "TeamInID",
                principalTable: "Teams",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CompetitionManager.Migrations
{
    public partial class AddTeamIDToCompetitorNotNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competitors_Teams_TeamInID",
                table: "Competitors");

            migrationBuilder.AlterColumn<int>(
                name: "TeamInID",
                table: "Competitors",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Competitors_Teams_TeamInID",
                table: "Competitors",
                column: "TeamInID",
                principalTable: "Teams",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competitors_Teams_TeamInID",
                table: "Competitors");

            migrationBuilder.AlterColumn<int>(
                name: "TeamInID",
                table: "Competitors",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Competitors_Teams_TeamInID",
                table: "Competitors",
                column: "TeamInID",
                principalTable: "Teams",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

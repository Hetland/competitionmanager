﻿namespace CompetitionManager
{
    partial class EditCompetitorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.createCompetitorsBox = new System.Windows.Forms.GroupBox();
            this.teamLabel = new System.Windows.Forms.Label();
            this.teamBox = new System.Windows.Forms.ComboBox();
            this.yearsPlayedInput = new System.Windows.Forms.NumericUpDown();
            this.yearsPlayedLabel = new System.Windows.Forms.Label();
            this.competitorTraitsListBox = new System.Windows.Forms.CheckedListBox();
            this.updateCompetitor = new System.Windows.Forms.Button();
            this.crossOffListLabel = new System.Windows.Forms.Label();
            this.cityOfResidenceLabel = new System.Windows.Forms.Label();
            this.cityOfResidenceBox = new System.Windows.Forms.ComboBox();
            this.nameOfCompetitorLabel = new System.Windows.Forms.Label();
            this.createCompetitorsBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.yearsPlayedInput)).BeginInit();
            this.SuspendLayout();
            // 
            // createCompetitorsBox
            // 
            this.createCompetitorsBox.Controls.Add(this.teamLabel);
            this.createCompetitorsBox.Controls.Add(this.teamBox);
            this.createCompetitorsBox.Controls.Add(this.yearsPlayedInput);
            this.createCompetitorsBox.Controls.Add(this.yearsPlayedLabel);
            this.createCompetitorsBox.Controls.Add(this.competitorTraitsListBox);
            this.createCompetitorsBox.Controls.Add(this.updateCompetitor);
            this.createCompetitorsBox.Controls.Add(this.crossOffListLabel);
            this.createCompetitorsBox.Controls.Add(this.cityOfResidenceLabel);
            this.createCompetitorsBox.Controls.Add(this.cityOfResidenceBox);
            this.createCompetitorsBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.createCompetitorsBox.Location = new System.Drawing.Point(32, 63);
            this.createCompetitorsBox.Name = "createCompetitorsBox";
            this.createCompetitorsBox.Size = new System.Drawing.Size(321, 357);
            this.createCompetitorsBox.TabIndex = 13;
            this.createCompetitorsBox.TabStop = false;
            // 
            // teamLabel
            // 
            this.teamLabel.AutoSize = true;
            this.teamLabel.Location = new System.Drawing.Point(6, 92);
            this.teamLabel.Name = "teamLabel";
            this.teamLabel.Size = new System.Drawing.Size(34, 13);
            this.teamLabel.TabIndex = 13;
            this.teamLabel.Text = "Team";
            // 
            // teamBox
            // 
            this.teamBox.FormattingEnabled = true;
            this.teamBox.Location = new System.Drawing.Point(112, 89);
            this.teamBox.Name = "teamBox";
            this.teamBox.Size = new System.Drawing.Size(199, 21);
            this.teamBox.TabIndex = 12;
            // 
            // yearsPlayedInput
            // 
            this.yearsPlayedInput.Location = new System.Drawing.Point(112, 65);
            this.yearsPlayedInput.Name = "yearsPlayedInput";
            this.yearsPlayedInput.Size = new System.Drawing.Size(199, 20);
            this.yearsPlayedInput.TabIndex = 11;
            // 
            // yearsPlayedLabel
            // 
            this.yearsPlayedLabel.AutoSize = true;
            this.yearsPlayedLabel.Location = new System.Drawing.Point(6, 69);
            this.yearsPlayedLabel.Name = "yearsPlayedLabel";
            this.yearsPlayedLabel.Size = new System.Drawing.Size(102, 13);
            this.yearsPlayedLabel.TabIndex = 10;
            this.yearsPlayedLabel.Text = "Years played games";
            // 
            // competitorTraitsListBox
            // 
            this.competitorTraitsListBox.FormattingEnabled = true;
            this.competitorTraitsListBox.Items.AddRange(new object[] {
            "Good With Computers",
            "Is An Introvert",
            "Likes Manga/Anime",
            "Loves SciFi",
            "Collects Cards Of Some Form",
            "Lives At Home",
            "Has Girlfriend",
            "Loves Science"});
            this.competitorTraitsListBox.Location = new System.Drawing.Point(112, 116);
            this.competitorTraitsListBox.Name = "competitorTraitsListBox";
            this.competitorTraitsListBox.Size = new System.Drawing.Size(199, 124);
            this.competitorTraitsListBox.TabIndex = 0;
            // 
            // updateCompetitor
            // 
            this.updateCompetitor.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateCompetitor.Location = new System.Drawing.Point(112, 301);
            this.updateCompetitor.Name = "updateCompetitor";
            this.updateCompetitor.Size = new System.Drawing.Size(199, 46);
            this.updateCompetitor.TabIndex = 1;
            this.updateCompetitor.Text = "Update competitor details";
            this.updateCompetitor.UseVisualStyleBackColor = true;
            this.updateCompetitor.Click += new System.EventHandler(this.updateCompetitor_Click);
            // 
            // crossOffListLabel
            // 
            this.crossOffListLabel.AutoSize = true;
            this.crossOffListLabel.Location = new System.Drawing.Point(8, 122);
            this.crossOffListLabel.Name = "crossOffListLabel";
            this.crossOffListLabel.Size = new System.Drawing.Size(33, 13);
            this.crossOffListLabel.TabIndex = 8;
            this.crossOffListLabel.Text = "Traits";
            // 
            // cityOfResidenceLabel
            // 
            this.cityOfResidenceLabel.AutoSize = true;
            this.cityOfResidenceLabel.Location = new System.Drawing.Point(6, 45);
            this.cityOfResidenceLabel.Name = "cityOfResidenceLabel";
            this.cityOfResidenceLabel.Size = new System.Drawing.Size(90, 13);
            this.cityOfResidenceLabel.TabIndex = 7;
            this.cityOfResidenceLabel.Text = "City of Residence";
            // 
            // cityOfResidenceBox
            // 
            this.cityOfResidenceBox.FormattingEnabled = true;
            this.cityOfResidenceBox.Location = new System.Drawing.Point(112, 41);
            this.cityOfResidenceBox.Name = "cityOfResidenceBox";
            this.cityOfResidenceBox.Size = new System.Drawing.Size(199, 21);
            this.cityOfResidenceBox.TabIndex = 4;
            // 
            // nameOfCompetitorLabel
            // 
            this.nameOfCompetitorLabel.AutoSize = true;
            this.nameOfCompetitorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameOfCompetitorLabel.Location = new System.Drawing.Point(102, 27);
            this.nameOfCompetitorLabel.Name = "nameOfCompetitorLabel";
            this.nameOfCompetitorLabel.Size = new System.Drawing.Size(192, 24);
            this.nameOfCompetitorLabel.TabIndex = 12;
            this.nameOfCompetitorLabel.Text = "Name of Contestant";
            // 
            // EditCompetitorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 452);
            this.Controls.Add(this.createCompetitorsBox);
            this.Controls.Add(this.nameOfCompetitorLabel);
            this.Name = "EditCompetitorForm";
            this.Text = "EditCompetitorForm";
            this.createCompetitorsBox.ResumeLayout(false);
            this.createCompetitorsBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.yearsPlayedInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox createCompetitorsBox;
        private System.Windows.Forms.Label teamLabel;
        private System.Windows.Forms.ComboBox teamBox;
        private System.Windows.Forms.NumericUpDown yearsPlayedInput;
        private System.Windows.Forms.Label yearsPlayedLabel;
        private System.Windows.Forms.CheckedListBox competitorTraitsListBox;
        private System.Windows.Forms.Button updateCompetitor;
        private System.Windows.Forms.Label crossOffListLabel;
        private System.Windows.Forms.Label cityOfResidenceLabel;
        private System.Windows.Forms.ComboBox cityOfResidenceBox;
        private System.Windows.Forms.Label nameOfCompetitorLabel;
    }
}
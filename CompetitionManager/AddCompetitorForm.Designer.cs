﻿namespace CompetitionManager
{
    partial class AddCompetitorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.competitorTraitsListBox = new System.Windows.Forms.CheckedListBox();
            this.createNewCompetitor = new System.Windows.Forms.Button();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.dobPicker = new System.Windows.Forms.DateTimePicker();
            this.cityOfResidenceBox = new System.Windows.Forms.ComboBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.dobLabel = new System.Windows.Forms.Label();
            this.cityOfResidenceLabel = new System.Windows.Forms.Label();
            this.crossOffListLabel = new System.Windows.Forms.Label();
            this.createCompetitorLabel = new System.Windows.Forms.Label();
            this.createCompetitorsBox = new System.Windows.Forms.GroupBox();
            this.teamLabel = new System.Windows.Forms.Label();
            this.teamBox = new System.Windows.Forms.ComboBox();
            this.yearsPlayedInput = new System.Windows.Forms.NumericUpDown();
            this.yearsPlayedLabel = new System.Windows.Forms.Label();
            this.competitorsBox = new System.Windows.Forms.GroupBox();
            this.deleteCompetitorButton = new System.Windows.Forms.Button();
            this.editCompetitorButton = new System.Windows.Forms.Button();
            this.allCompetitorsListBox = new System.Windows.Forms.ListView();
            this.NameCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DOB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.City = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.YearsPlayed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Traits = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TeamIn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.competitorBoxLabel = new System.Windows.Forms.Label();
            this.viewTeamsButton = new System.Windows.Forms.Button();
            this.createCompetitorsBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.yearsPlayedInput)).BeginInit();
            this.competitorsBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // competitorTraitsListBox
            // 
            this.competitorTraitsListBox.FormattingEnabled = true;
            this.competitorTraitsListBox.Items.AddRange(new object[] {
            "Good With Computers",
            "Is An Introvert",
            "Likes Manga/Anime",
            "Loves SciFi",
            "Collects Cards Of Some Form",
            "Lives At Home",
            "Has Girlfriend",
            "Loves Science"});
            this.competitorTraitsListBox.Location = new System.Drawing.Point(112, 150);
            this.competitorTraitsListBox.Name = "competitorTraitsListBox";
            this.competitorTraitsListBox.Size = new System.Drawing.Size(238, 124);
            this.competitorTraitsListBox.TabIndex = 0;
            // 
            // createNewCompetitor
            // 
            this.createNewCompetitor.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createNewCompetitor.Location = new System.Drawing.Point(112, 301);
            this.createNewCompetitor.Name = "createNewCompetitor";
            this.createNewCompetitor.Size = new System.Drawing.Size(238, 46);
            this.createNewCompetitor.TabIndex = 1;
            this.createNewCompetitor.Text = "Create a new competitor";
            this.createNewCompetitor.UseVisualStyleBackColor = true;
            this.createNewCompetitor.Click += new System.EventHandler(this.createNewCompetitor_Click);
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(112, 12);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(238, 20);
            this.nameTextBox.TabIndex = 2;
            // 
            // dobPicker
            // 
            this.dobPicker.Location = new System.Drawing.Point(112, 35);
            this.dobPicker.Name = "dobPicker";
            this.dobPicker.Size = new System.Drawing.Size(238, 20);
            this.dobPicker.TabIndex = 3;
            // 
            // cityOfResidenceBox
            // 
            this.cityOfResidenceBox.FormattingEnabled = true;
            this.cityOfResidenceBox.Location = new System.Drawing.Point(112, 58);
            this.cityOfResidenceBox.Name = "cityOfResidenceBox";
            this.cityOfResidenceBox.Size = new System.Drawing.Size(238, 21);
            this.cityOfResidenceBox.TabIndex = 4;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(6, 15);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(35, 13);
            this.nameLabel.TabIndex = 5;
            this.nameLabel.Text = "Name";
            // 
            // dobLabel
            // 
            this.dobLabel.AutoSize = true;
            this.dobLabel.Location = new System.Drawing.Point(6, 39);
            this.dobLabel.Name = "dobLabel";
            this.dobLabel.Size = new System.Drawing.Size(65, 13);
            this.dobLabel.TabIndex = 6;
            this.dobLabel.Text = "Date of birth";
            // 
            // cityOfResidenceLabel
            // 
            this.cityOfResidenceLabel.AutoSize = true;
            this.cityOfResidenceLabel.Location = new System.Drawing.Point(6, 62);
            this.cityOfResidenceLabel.Name = "cityOfResidenceLabel";
            this.cityOfResidenceLabel.Size = new System.Drawing.Size(90, 13);
            this.cityOfResidenceLabel.TabIndex = 7;
            this.cityOfResidenceLabel.Text = "City of Residence";
            // 
            // crossOffListLabel
            // 
            this.crossOffListLabel.AutoSize = true;
            this.crossOffListLabel.Location = new System.Drawing.Point(8, 156);
            this.crossOffListLabel.Name = "crossOffListLabel";
            this.crossOffListLabel.Size = new System.Drawing.Size(33, 13);
            this.crossOffListLabel.TabIndex = 8;
            this.crossOffListLabel.Text = "Traits";
            // 
            // createCompetitorLabel
            // 
            this.createCompetitorLabel.AutoSize = true;
            this.createCompetitorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createCompetitorLabel.Location = new System.Drawing.Point(466, 4);
            this.createCompetitorLabel.Name = "createCompetitorLabel";
            this.createCompetitorLabel.Size = new System.Drawing.Size(253, 24);
            this.createCompetitorLabel.TabIndex = 9;
            this.createCompetitorLabel.Text = "Sheet for new competitors";
            // 
            // createCompetitorsBox
            // 
            this.createCompetitorsBox.Controls.Add(this.teamLabel);
            this.createCompetitorsBox.Controls.Add(this.teamBox);
            this.createCompetitorsBox.Controls.Add(this.yearsPlayedInput);
            this.createCompetitorsBox.Controls.Add(this.yearsPlayedLabel);
            this.createCompetitorsBox.Controls.Add(this.competitorTraitsListBox);
            this.createCompetitorsBox.Controls.Add(this.nameLabel);
            this.createCompetitorsBox.Controls.Add(this.createNewCompetitor);
            this.createCompetitorsBox.Controls.Add(this.nameTextBox);
            this.createCompetitorsBox.Controls.Add(this.crossOffListLabel);
            this.createCompetitorsBox.Controls.Add(this.dobPicker);
            this.createCompetitorsBox.Controls.Add(this.cityOfResidenceLabel);
            this.createCompetitorsBox.Controls.Add(this.cityOfResidenceBox);
            this.createCompetitorsBox.Controls.Add(this.dobLabel);
            this.createCompetitorsBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.createCompetitorsBox.Location = new System.Drawing.Point(434, 32);
            this.createCompetitorsBox.Name = "createCompetitorsBox";
            this.createCompetitorsBox.Size = new System.Drawing.Size(354, 357);
            this.createCompetitorsBox.TabIndex = 11;
            this.createCompetitorsBox.TabStop = false;
            // 
            // teamLabel
            // 
            this.teamLabel.AutoSize = true;
            this.teamLabel.Location = new System.Drawing.Point(6, 109);
            this.teamLabel.Name = "teamLabel";
            this.teamLabel.Size = new System.Drawing.Size(34, 13);
            this.teamLabel.TabIndex = 13;
            this.teamLabel.Text = "Team";
            // 
            // teamBox
            // 
            this.teamBox.FormattingEnabled = true;
            this.teamBox.Location = new System.Drawing.Point(112, 106);
            this.teamBox.Name = "teamBox";
            this.teamBox.Size = new System.Drawing.Size(238, 21);
            this.teamBox.TabIndex = 12;
            // 
            // yearsPlayedInput
            // 
            this.yearsPlayedInput.Location = new System.Drawing.Point(112, 82);
            this.yearsPlayedInput.Name = "yearsPlayedInput";
            this.yearsPlayedInput.Size = new System.Drawing.Size(238, 20);
            this.yearsPlayedInput.TabIndex = 11;
            // 
            // yearsPlayedLabel
            // 
            this.yearsPlayedLabel.AutoSize = true;
            this.yearsPlayedLabel.Location = new System.Drawing.Point(6, 86);
            this.yearsPlayedLabel.Name = "yearsPlayedLabel";
            this.yearsPlayedLabel.Size = new System.Drawing.Size(102, 13);
            this.yearsPlayedLabel.TabIndex = 10;
            this.yearsPlayedLabel.Text = "Years played games";
            // 
            // competitorsBox
            // 
            this.competitorsBox.Controls.Add(this.deleteCompetitorButton);
            this.competitorsBox.Controls.Add(this.editCompetitorButton);
            this.competitorsBox.Controls.Add(this.allCompetitorsListBox);
            this.competitorsBox.Location = new System.Drawing.Point(24, 33);
            this.competitorsBox.Name = "competitorsBox";
            this.competitorsBox.Size = new System.Drawing.Size(395, 357);
            this.competitorsBox.TabIndex = 12;
            this.competitorsBox.TabStop = false;
            // 
            // deleteCompetitorButton
            // 
            this.deleteCompetitorButton.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteCompetitorButton.Location = new System.Drawing.Point(208, 317);
            this.deleteCompetitorButton.Name = "deleteCompetitorButton";
            this.deleteCompetitorButton.Size = new System.Drawing.Size(181, 34);
            this.deleteCompetitorButton.TabIndex = 10;
            this.deleteCompetitorButton.Text = "Delete";
            this.deleteCompetitorButton.UseVisualStyleBackColor = true;
            this.deleteCompetitorButton.Click += new System.EventHandler(this.deleteCompetitorButton_Click);
            // 
            // editCompetitorButton
            // 
            this.editCompetitorButton.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editCompetitorButton.Location = new System.Drawing.Point(6, 317);
            this.editCompetitorButton.Name = "editCompetitorButton";
            this.editCompetitorButton.Size = new System.Drawing.Size(196, 34);
            this.editCompetitorButton.TabIndex = 9;
            this.editCompetitorButton.Text = "Edit";
            this.editCompetitorButton.UseVisualStyleBackColor = true;
            this.editCompetitorButton.Click += new System.EventHandler(this.editCompetitorButton_Click);
            // 
            // allCompetitorsListBox
            // 
            this.allCompetitorsListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.NameCol,
            this.DOB,
            this.City,
            this.YearsPlayed,
            this.Traits,
            this.TeamIn});
            this.allCompetitorsListBox.FullRowSelect = true;
            this.allCompetitorsListBox.HideSelection = false;
            this.allCompetitorsListBox.Location = new System.Drawing.Point(6, 14);
            this.allCompetitorsListBox.MultiSelect = false;
            this.allCompetitorsListBox.Name = "allCompetitorsListBox";
            this.allCompetitorsListBox.Size = new System.Drawing.Size(383, 297);
            this.allCompetitorsListBox.TabIndex = 0;
            this.allCompetitorsListBox.UseCompatibleStateImageBehavior = false;
            // 
            // NameCol
            // 
            this.NameCol.Text = "Name";
            // 
            // DOB
            // 
            this.DOB.Text = "DOB";
            // 
            // City
            // 
            this.City.Text = "City";
            // 
            // YearsPlayed
            // 
            this.YearsPlayed.Text = "Years played games";
            // 
            // Traits
            // 
            this.Traits.Text = "Number of traits";
            // 
            // TeamIn
            // 
            this.TeamIn.Text = "Team";
            // 
            // competitorBoxLabel
            // 
            this.competitorBoxLabel.AutoSize = true;
            this.competitorBoxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.competitorBoxLabel.Location = new System.Drawing.Point(40, 5);
            this.competitorBoxLabel.Name = "competitorBoxLabel";
            this.competitorBoxLabel.Size = new System.Drawing.Size(148, 24);
            this.competitorBoxLabel.TabIndex = 13;
            this.competitorBoxLabel.Text = "All competitors";
            // 
            // viewTeamsButton
            // 
            this.viewTeamsButton.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewTeamsButton.Location = new System.Drawing.Point(24, 397);
            this.viewTeamsButton.Name = "viewTeamsButton";
            this.viewTeamsButton.Size = new System.Drawing.Size(395, 44);
            this.viewTeamsButton.TabIndex = 11;
            this.viewTeamsButton.Text = "View teams";
            this.viewTeamsButton.UseVisualStyleBackColor = true;
            this.viewTeamsButton.Click += new System.EventHandler(this.viewTeamsButton_Click);
            // 
            // AddCompetitorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.viewTeamsButton);
            this.Controls.Add(this.competitorBoxLabel);
            this.Controls.Add(this.competitorsBox);
            this.Controls.Add(this.createCompetitorsBox);
            this.Controls.Add(this.createCompetitorLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "AddCompetitorForm";
            this.Text = "Who Is The Biggest Nerd Around?";
            this.Load += new System.EventHandler(this.AddCompetitorForm_Load);
            this.createCompetitorsBox.ResumeLayout(false);
            this.createCompetitorsBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.yearsPlayedInput)).EndInit();
            this.competitorsBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox competitorTraitsListBox;
        private System.Windows.Forms.Button createNewCompetitor;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.DateTimePicker dobPicker;
        private System.Windows.Forms.ComboBox cityOfResidenceBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label dobLabel;
        private System.Windows.Forms.Label cityOfResidenceLabel;
        private System.Windows.Forms.Label crossOffListLabel;
        private System.Windows.Forms.Label createCompetitorLabel;
        private System.Windows.Forms.GroupBox createCompetitorsBox;
        private System.Windows.Forms.GroupBox competitorsBox;
        private System.Windows.Forms.Label competitorBoxLabel;
        private System.Windows.Forms.Button deleteCompetitorButton;
        private System.Windows.Forms.Button editCompetitorButton;
        private System.Windows.Forms.ListView allCompetitorsListBox;
        private System.Windows.Forms.Label yearsPlayedLabel;
        private System.Windows.Forms.ColumnHeader NameCol;
        private System.Windows.Forms.ColumnHeader DOB;
        private System.Windows.Forms.ColumnHeader City;
        private System.Windows.Forms.ColumnHeader YearsPlayed;
        private System.Windows.Forms.ColumnHeader Traits;
        private System.Windows.Forms.NumericUpDown yearsPlayedInput;
        private System.Windows.Forms.ColumnHeader TeamIn;
        private System.Windows.Forms.Label teamLabel;
        private System.Windows.Forms.ComboBox teamBox;
        private System.Windows.Forms.Button viewTeamsButton;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompetitionManager
{
    public class Coach
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Team TeamToSupervise { get; set; }
        public int TeamToSuperviseId { get; set; }

        public Coach()
        {
            Name = "Mr. Bear";
        }

        public Coach(string name)
        {
            Name = name;
        }

        public Coach(string name, Team teamToSupervise)
        {
            Name = name;
            TeamToSupervise = teamToSupervise;
        }
    }
}

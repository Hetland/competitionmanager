﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompetitionManager
{
    public partial class AddCompetitorForm : Form
    {
        AddCompetitorFormLogic logic;

        public AddCompetitorForm()
        {
            InitializeComponent();
            logic = new AddCompetitorFormLogic();
            allCompetitorsListBox.View = View.Details;

            ShowAllCompetitors();
            ShowCitiesOnCompetitorCreation();
            ShowTeamsOnCompetitorCreation();
            

        }

        private void AddCompetitorForm_Load(object sender, EventArgs e)
        {

        }

        private void createNewCompetitor_Click(object sender, EventArgs e)
        {
            string name = nameTextBox.Text;
            string teamName = teamBox.Text;
            DateTime dob = dobPicker.Value;
            string residence = cityOfResidenceBox.Text;
            int yrsPlayed = Decimal.ToInt32(yearsPlayedInput.Value);

            // If user hasn't entered valid info, do not save to db
            if (name == "" || teamName == "")
            {
                return;
            }

            Team teamIn = AddCompetitorFormLogic.GetATeam(teamBox.Text);
            bool[] traitsTrueOrNot = new bool[competitorTraitsListBox.Items.Count];
            for (int traitNum = 0; traitNum < competitorTraitsListBox.Items.Count; traitNum++)
            {
                bool isChecked = (competitorTraitsListBox.GetItemCheckState(traitNum) == CheckState.Checked);
                traitsTrueOrNot[traitNum] = isChecked;
            }
            
            logic.CreateCompetitor( name, 
                                    dob, 
                                    residence, 
                                    yrsPlayed, 
                                    traitsTrueOrNot[0], 
                                    traitsTrueOrNot[1], 
                                    traitsTrueOrNot[2], 
                                    traitsTrueOrNot[3], 
                                    traitsTrueOrNot[4], 
                                    traitsTrueOrNot[5], 
                                    traitsTrueOrNot[6], 
                                    traitsTrueOrNot[7]);
            logic.AddCompetitorToDB(teamName);
            ShowAllCompetitors();
        }

        private void editCompetitorButton_Click(object sender, EventArgs e)
        {
            string competitorName;
            if (allCompetitorsListBox.SelectedItems.Count > 0)
            {
                competitorName = allCompetitorsListBox.SelectedItems[0].Text;
                EditCompetitorForm editCompetitorForm = new EditCompetitorForm(competitorName);
                editCompetitorForm.ShowDialog();
                ShowAllCompetitors();
            }
        }
        private void deleteCompetitorButton_Click(object sender, EventArgs e)
        {
            string competitorName;
            if (allCompetitorsListBox.SelectedItems.Count > 0)
            {
                competitorName = allCompetitorsListBox.SelectedItems[0].Text;
                using (CompetitorContext competitorContext = new CompetitorContext())
                {
                    Competitor competitorToDelete = competitorContext.Competitors.Where(competitor => competitor.Name == competitorName).First();
                    competitorContext.Competitors.Remove(competitorToDelete);
                    competitorContext.SaveChanges();
                }
                ShowAllCompetitors();
            }
        }

        private void viewTeamsButton_Click(object sender, EventArgs e)
        {
            TeamsForm teamsForm = new TeamsForm();
            teamsForm.ShowDialog();
            ShowAllCompetitors();
        }

        private void ShowAllCompetitors()
        {
            if (allCompetitorsListBox.Items.Count != 0)
            {
                allCompetitorsListBox.Items.Clear();
            }
            List<Competitor> allCompetitors = logic.GetAllCompetitors();
            foreach (Competitor competitor in allCompetitors)
            {
                Team teamCompetitorIsIn = AddCompetitorFormLogic.GetATeamByID(competitor.TeamId);
                int numOfTraitsTrue = AddCompetitorFormLogic.NumOfTraitsTrue(competitor);
                string[] competitorRelevantInfo = { competitor.Name,
                                                    competitor.DOB.ToString(),
                                                    competitor.CurrentResidence,
                                                    competitor.YearsPlayedVideogames.ToString(),
                                                    numOfTraitsTrue.ToString(),
                                                    teamCompetitorIsIn.Name
                };
                allCompetitorsListBox.Items.Add(new ListViewItem(competitorRelevantInfo));
            }
        }

        private void ShowCitiesOnCompetitorCreation()
        {
            string[] allCities = AddCompetitorFormLogic.GetAllCities().ToArray();
            cityOfResidenceBox.Items.AddRange(allCities);
        }

        private void ShowTeamsOnCompetitorCreation()
        {
            string[] allTeams = AddCompetitorFormLogic.GetAllTeams().ToArray();
            teamBox.Items.AddRange(allTeams);
        }
    }
}

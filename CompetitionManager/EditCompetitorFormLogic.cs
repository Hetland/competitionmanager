﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompetitionManager
{
    public class EditCompetitorFormLogic
    {
        public static Competitor GetCompetitor(string competitorName)
        {
            Competitor competitor = new Competitor();

            using (CompetitorContext competitorContext = new CompetitorContext())
            {
                Competitor foundCompetitor = competitorContext.Competitors.Where(comp => comp.Name == competitorName).First();
                competitor = foundCompetitor;
            }

            return competitor;
        }

        public static void UpdateCompetitorInDB(    string name,
                                                    string residence,
                                                    float yrsPlayed,
                                                    string teamName,
                                                    bool goodWithComputers,
                                                    bool introvert,
                                                    bool mangaOrAnime,
                                                    bool lovesSciFi,
                                                    bool collectsCards,
                                                    bool livesAtHome,
                                                    bool hasGF,
                                                    bool lovesScience)
        {
            MakeTeamIfDoesntExist(teamName);
            using (CompetitorContext competitorContext = new CompetitorContext())
            {
                Team teamToUse = competitorContext.Teams.Where(team => team.Name == teamName).First();
                Competitor competitorToUpdate = competitorContext.Competitors.Where(comp => comp.Name == name).First();
                competitorToUpdate.CurrentResidence = residence;
                competitorToUpdate.YearsPlayedVideogames = yrsPlayed;
                competitorToUpdate.Team = teamToUse;
                competitorToUpdate.CollectsCards = collectsCards;
                competitorToUpdate.GoodWithComputers = goodWithComputers;
                competitorToUpdate.HasGirlfriend = hasGF;
                competitorToUpdate.IsIntrovert = introvert;
                competitorToUpdate.LivesAtHome = livesAtHome;
                competitorToUpdate.LovesScience = lovesScience;
                competitorToUpdate.LovesSciFi = lovesSciFi;
                competitorToUpdate.MangaOrAnime = mangaOrAnime;
                competitorContext.SaveChanges();
            }
        }

        public static void MakeTeamIfDoesntExist(string teamName)
        {
            using (CompetitorContext competitorContext = new CompetitorContext())
            {
                if (competitorContext.Teams.Where(t => t.Name == teamName).Count() == 0)
                {
                    Team newTeam = new Team(teamName);
                    competitorContext.Teams.Add(newTeam);
                    competitorContext.SaveChanges();
                }
            }
        }


    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompetitionManager
{
    public class Competitor
    {
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        public DateTime DOB { get; set; }

        public string CurrentResidence { get; set; }

        public float YearsPlayedVideogames { get; set; }

        public bool GoodWithComputers { get; set; }

        public bool IsIntrovert {get; set; }

        public bool MangaOrAnime { get; set; }

        public bool LovesSciFi { get; set; }

        public bool CollectsCards { get; set; }

        public bool LivesAtHome { get; set; }

        public bool HasGirlfriend { get; set; }

        public bool LovesScience { get; set; }

        public Team Team { get; set; }

        [ForeignKey("Team")]
        public int TeamId { get; set; }

        public ICollection<SkillCompetitor> CompetitorSkills { get; set; }

        public Competitor()
            {
                Name = "Joe Piscattio";
                DOB = new DateTime(1882, 3, 5);
                CurrentResidence = "Stavanger";
                YearsPlayedVideogames = 15f;
                GoodWithComputers = false;
                IsIntrovert = true;
                MangaOrAnime = true;
                LovesSciFi = true;
                CollectsCards = false;
                LivesAtHome = false;
                HasGirlfriend = false;
                LovesScience = true;
            }

        public Competitor(  string name, 
                            DateTime dob, 
                            string residence, 
                            float yearsPlayed, 
                            bool goodWithComputers, 
                            bool introvert, 
                            bool mangaOrAnime, 
                            bool lovesSciFi, 
                            bool collectsCards,
                            bool livesAtHome, 
                            bool hasGF,
                            bool lovesScience)
            {
                Name = name;
                DOB = dob;
                CurrentResidence = residence;
                YearsPlayedVideogames = yearsPlayed;
                GoodWithComputers = goodWithComputers;
                IsIntrovert = introvert;
                MangaOrAnime = mangaOrAnime;
                LovesSciFi = lovesSciFi;
                CollectsCards = collectsCards;
                LivesAtHome = livesAtHome;
                HasGirlfriend = hasGF;
                LovesScience = lovesScience;
            }
    }
}

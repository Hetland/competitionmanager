﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompetitionManager
{
    public partial class EditCompetitorForm : Form
    {

        public EditCompetitorForm(string competitorName)
        {
            InitializeComponent();
            GetCompetitorInfo(competitorName);

        }

        private void GetCompetitorInfo(string competitorName)
        {
            Competitor competitorToEdit = EditCompetitorFormLogic.GetCompetitor(competitorName);
            nameOfCompetitorLabel.Text = competitorToEdit.Name;
            cityOfResidenceBox.Text = competitorToEdit.CurrentResidence;
            yearsPlayedInput.Value = (int) competitorToEdit.YearsPlayedVideogames;
            teamBox.Text = GetNameOfCompetitorsTeam(competitorToEdit.TeamId);
        }

        private string GetNameOfCompetitorsTeam(int teamID)
        {
            string nameOfTeam = "";
            using (CompetitorContext competitorContext = new CompetitorContext())
            {
                Team teamOfCompetitor = competitorContext.Teams.Find(teamID);
                nameOfTeam = teamOfCompetitor.Name;
            }

            return nameOfTeam;
        }

        private void ShowCitiesOnCompetitorCreation()
        {

            string[] allCities = AddCompetitorFormLogic.GetAllCities().ToArray();
            cityOfResidenceBox.Items.AddRange(allCities);
        }
        private void ShowTeamsOnCompetitorCreation()
        {
            string[] allTeams = AddCompetitorFormLogic.GetAllTeams().ToArray();
            teamBox.Items.AddRange(allTeams);
        }

        private void updateCompetitor_Click(object sender, EventArgs e)
        {
                string name = nameOfCompetitorLabel.Text;
                string teamName = teamBox.Text;
                string residence = cityOfResidenceBox.Text;
                int yrsPlayed = Decimal.ToInt32(yearsPlayedInput.Value);

                // If user hasn't entered valid info, do not save to db
                if (teamName == "")
                {
                    return;
                }

                Team teamIn = AddCompetitorFormLogic.GetATeam(teamBox.Text);
                bool[] traitsTrueOrNot = new bool[competitorTraitsListBox.Items.Count];
                for (int traitNum = 0; traitNum < competitorTraitsListBox.Items.Count; traitNum++)
                {
                    bool isChecked = (competitorTraitsListBox.GetItemCheckState(traitNum) == CheckState.Checked);
                    traitsTrueOrNot[traitNum] = isChecked;
                }

                EditCompetitorFormLogic.UpdateCompetitorInDB(   name,
                                                                residence,
                                                                yrsPlayed,
                                                                teamName,
                                                                traitsTrueOrNot[0],
                                                                traitsTrueOrNot[1],
                                                                traitsTrueOrNot[2],
                                                                traitsTrueOrNot[3],
                                                                traitsTrueOrNot[4],
                                                                traitsTrueOrNot[5],
                                                                traitsTrueOrNot[6],
                                                                traitsTrueOrNot[7]);
        }
    }
}

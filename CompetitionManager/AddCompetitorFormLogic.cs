﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompetitionManager
{
    class AddCompetitorFormLogic
    {
        private Competitor newCompetitor;

        public void CreateCompetitor()
        {
            newCompetitor = new Competitor();
        }

        public void CreateCompetitor(   string name,
                                        DateTime dob,
                                        string residence,
                                        float yearsPlayed,
                                        bool goodWithComputers,
                                        bool introvert,
                                        bool mangaOrAnime,
                                        bool lovesSciFi,
                                        bool collectsCards,
                                        bool livesAtHome,
                                        bool hasGF,
                                        bool lovesScience)
        {

            newCompetitor = new Competitor( name,
                                            dob,
                                            residence,
                                            yearsPlayed,
                                            goodWithComputers,
                                            introvert,
                                            mangaOrAnime,
                                            lovesSciFi,
                                            collectsCards,
                                            livesAtHome,
                                            hasGF,
                                            lovesScience);
        }


        public void AddCompetitorToDB(string teamName)
        {
            using (CompetitorContext competitorContext = new CompetitorContext())
            {
                //Need to use using to add relation to the Team
                //Inside using, find the Team and add this to newCompetitor or smth
                Team teamCompetitorIsIn = competitorContext.Teams.Where(team => team.Name == teamName).Single();
                newCompetitor.Team = teamCompetitorIsIn;
                competitorContext.Competitors.Add(newCompetitor);
                teamCompetitorIsIn.Competitors.Add(newCompetitor);
                competitorContext.SaveChanges();
            }
        }

        public List<Competitor> GetAllCompetitors()
        {
            List<Competitor> allCompetitors;

            using (CompetitorContext competitorContext = new CompetitorContext())
            {
                allCompetitors = competitorContext.Competitors.ToList();
            }

            return allCompetitors;
        }

        public static int NumOfTraitsTrue(Competitor competitor)
        {
            int numTraitsTrue = 0;
            numTraitsTrue += competitor.CollectsCards ? 1 : 0;
            numTraitsTrue += competitor.GoodWithComputers ? 1 : 0;
            numTraitsTrue += competitor.HasGirlfriend ? 0 : 1;
            numTraitsTrue += competitor.IsIntrovert ? 1 : 0;
            numTraitsTrue += competitor.LivesAtHome ? 1 : 0;
            numTraitsTrue += competitor.LovesScience ? 1 : 0;
            numTraitsTrue += competitor.LovesSciFi ? 1 : 0;
            numTraitsTrue += competitor.MangaOrAnime ? 1 : 0;

            return numTraitsTrue;
        }

        public static List<string> GetAllCities()
        {
            List<string> cities = new List<string>();

            using (var reader = new StreamReader(@"CitiesInNorway.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    cities.Add(values[0]);
                }
            }
            return cities;
        }

        public static List<string> GetAllTeams()
        {
            List<Team> allTeams = new List<Team>();
            
            using (CompetitorContext competitorContext = new CompetitorContext())
            {
                allTeams = competitorContext.Teams.ToList();
            }

            List<string> allTeamNames = new List<string>();
            foreach (Team team in allTeams)
            {
                allTeamNames.Add(team.Name);
            }
            return allTeamNames;
        }

        public static Team GetATeam(string teamName)
        {
            Team team;

            using (CompetitorContext competitorContext = new CompetitorContext())
            {
                Team[] foundTeam = competitorContext.Teams.Where(aTeam => aTeam.Name == teamName).ToArray();
                if (foundTeam.Length == 0)
                {
                    team = new Team(teamName);
                    team.Competitors = new List<Competitor>();
                    competitorContext.Teams.Add(team);
                    competitorContext.SaveChanges();
                }
                else
                {
                    team = foundTeam[0];
                }
            }
            return team;
        }

        public static Team GetATeamByID(int teamID)
        {
            Team team;

            using (CompetitorContext competitorContext = new CompetitorContext())
            {
                Team[] foundTeam = competitorContext.Teams.Where(aTeam => aTeam.ID == teamID).ToArray();
                team = foundTeam[0];
                
            }
            return team;
        }

        public static void SeedTeams()
        {
            using (CompetitorContext competitorContext = new CompetitorContext())
            {
                string[] seedTeamNames = { "Cookadoodles", "Werblers", "Schiftenschafterns", "Ausrausers" };
                foreach (string teamName in seedTeamNames)
                {
                    Team newTeam = new Team(teamName);
                    competitorContext.Teams.Add(newTeam);
                }
                competitorContext.SaveChanges();
            }
        }

        public static void SeedCoaches()
        {

        }

        public static void SeedSkills()
        {

        }

        public static void SeedCompetitors()
        {

        }
    }
}

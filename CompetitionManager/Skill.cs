﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompetitionManager
{
    public class Skill
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ICollection<SkillCompetitor> CompetitorSkills { get; set; }
    }
}
